package com.example.demo1;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/** класс доступа к БД */
public class UserDAO {

    /** метод получения объекта класса пользователя, получаемого из записей БД по имени пользователя */
    public static UserAccount findByName(String name) {
        // создание подкючения
        Connection connection = ConnectionManager.getConnection();
        // создание объекта класса пользователя
        UserAccount user = null;
        // блок try
        try {
            // строка запроса в БД
            PreparedStatement prepStatement
                    = connection.prepareStatement("SELECT DISTINCT name, password, gender FROM user WHERE name=?");
            // задание параметра этой строке
            prepStatement.setString(1, name);
            // выполнение запроса к БД
            ResultSet resObj = prepStatement.executeQuery();
            // цикл
            while (resObj.next()) {
                // преобразование записей БД в объект класса пользователя
                user = new UserAccount();
                String userName = resObj.getString("name");
                user.setUserName(userName);
                String password = resObj.getString("password");
                user.setPassword(password);
                String gender = resObj.getString("gender");
                user.setGender(gender);
            }
        // блок catch (перехватывающий SQLException)
        } catch (SQLException exception) {
            // вывод ошибки в консоль
            exception.printStackTrace();
        }
        return user;
    }
    /**
     * Try to find user with given name and password in database
     * @return user if userName and password correct, null otherwise
     */
    public static UserAccount findUser(String userName, String password) {
        UserAccount u = findByName(userName);
        if (u != null && u.getPassword().equals(password)) {
            return u;
        }
        return null;
    }
}
