package com.example.demo1;

import java.sql.*;

//класс подключения
public class ConnectionManager {

    // клиент для доступа к БД
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    // URI БД
    static final String JDBC_DB_URL = "jdbc:mysql://localhost:3306/ris";
    // мой ник
    static final String JDBC_USER = "root";
    // мой пароль
    static final String JDBC_PASS = "42iuhh2yzUQq";
    // переменная, хранящая подключение
    private static Connection connection = null;

    /** метод, возвращающий подключение к БД */
    static public Connection getConnection() {
        if (connection == null) {
            try {
                Class.forName(JDBC_DRIVER);
                connection = DriverManager.getConnection(JDBC_DB_URL, JDBC_USER, JDBC_PASS);
            } catch (SQLException | ClassNotFoundException exception) {
                exception.printStackTrace();
            }
        }
        return connection;
    }
}
