package com.example.demo1;

import java.io.*;
import javax.servlet.http.*;

public class HelloServlet extends HttpServlet {

    //объявление атрибутор класса
    private String message;

    //методы инициализации переменной message, которой присваивается строку
    @Override
    public void init() {
        message = "Hello Servlet!";
    }

    //метод отправки пользователю HTML-страницы
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");

        PrintWriter out = response.getWriter();
        out.println("<html><body>");
        out.println("<h1>" + message + "</h1>");
        out.println("</body></html>");
    }
}