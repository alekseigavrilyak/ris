package com.example.demo1;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

//класс аторизации
public class Login extends HttpServlet {

    //метод, считывающий данные, которые ввел пользователь, и заносящий их в нужную переменную
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String userName = request.getParameter("userName");
        String password = request.getParameter("password");

        //создание экземпляра класса, вызов метода проверки корректности вводимых данных
        UserAccount userAccount = UserDAO.findUser(userName, password);

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        //проверка правильности введённых данных
        if (userAccount == null) {
            String errorMessage = "Invalid userName or Password";
            out.println("<html><body>");
            out.println("<h1>" + errorMessage + "</h1>");
            out.println("</body></html>");
            return;
        }

        out.println("<html><body>");
        out.println("<h1>" + "Ok" + "</h1>");
        out.println("</body></html>");
    }

}